# XTP

Free VPN over HTTP Proxy for Android.

## Version
4.0

## Usage

1. Download and install xtp.apk
1. Open XTP and input config url
1. Click 'Update' and wait for complete
1. Click 'Enable' button to start VPN
